class VjotsIO : Object {

    public string readVjotsFile(string fn) {
        string read;

        try{
            FileUtils.get_contents (fn, out read);
	}
        catch (GLib.FileError e) {
            stderr.printf ("%s\n", e.message);
        }
	finally{}
        return read;
    }


    public void saveVjotsFile(string fn, string text) {
        try{
            FileUtils.set_contents (fn, text);
	}
        catch (GLib.FileError e) {
            stderr.printf ("%s\n", e.message);
            return;
        }
	finally{}
    }
}
