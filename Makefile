SUBDIRS=tests

.PHONY: resources subdirs

TARGET=vjots
RESOURCE=$(TARGET)-resource.xml

# make sure we rebuild each time
default: clean $(TARGET) subdirs

$(TARGET): resources.c
	valac --pkg gtk+-3.0 --pkg glib-2.0 resources.c  $(TARGET).vala vjotsdata.vala -o vjots

clean:
	rm -f $(TARGET) $(TARGET).debug resources.h resources.c core
	for dir in $(SUBDIRS); do \
		$(MAKE) clean -C $$dir; \
	done

c: resources.c resources.o
	gcc `pkg-config --cflags gtk+-3.0` -o $(TARGET)-c reyv.c `pkg-config --libs gtk+-3.0` resources.o

resources.o: resources.c
	gcc `pkg-config --cflags gtk+-3.0` -c resources.c `pkg-config --libs gtk+-3.0`

resources.c:
	glib-compile-resources --target=resources.c --generate-source $(RESOURCE)

resources.h:
	glib-compile-resources --target=resources.h --generate-header gjots2.xml

debug: resources.c resources.h vjots
	valac -D DEBUG --pkg gtk+-3.0 --pkg glib-2.0 $(TARGET).vala vjotsdata.vala vjotsdata_mock.vala resources.c -o $(TARGET).debug

subdirs:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done
