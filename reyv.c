#include <gtk/gtk.h>

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *grid;
  GError *err=NULL;

  gtk_init(NULL, NULL);

  // string newChildPath = "/vjots/resources/gjots2-new-child.png";
   // string newChildPath = "/vjots/resources/gjots2-new-child.png";
  // const char *path = "/reyv/resources/battery-icon.jpg";
  const char *path = "/reyv/resources/gjots2-new-child.png";
  GtkWidget *image;
  image = gtk_image_new_from_resource (path);

  if ( NULL != err){
    fprintf (stderr, "Error: %s\n", err->message);
    g_error_free (err);
  }

  /* Here we construct the container that is going pack our buttons */
  grid = gtk_grid_new ();

  window = gtk_application_window_new (app);

  /* Pack the container in the window */
  gtk_container_add (GTK_CONTAINER (window), grid);

  gtk_grid_attach (GTK_GRID (grid), image, 0, 0, 1, 1);

  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);
  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
// BEGIN GTK3 BOILERPLATE VARS
  GtkApplication *app;
  int status;
// END GTK3 BOILERPLATE VARS

// BEGIN GTK3 BOILERPLATE CODE
  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
// END GTK3 BOILERPLATE CODE

  return status;
}
