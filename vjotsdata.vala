class VjotsData : Object {
    public Gtk.TreeStore tree_store; // stores categories in left pane
    Gtk.TreeStore text_store; // stores actual text in right pane
    public Gtk.TreeView view;

    bool bNewTitle;

    // bFirst: are we on the 1st row of a NewFolder? If so, skip it
    // Otherwise, we get a blank line
    bool bFirst;

    // bRoot: Are we a NewEntry from the very first line?
    // If so, we need to make a NewFolder b/c gjots2
    // is inconsistent.
    // It treats the root node as special in which all the 1st level
    // Entries are beneath it and not on its same level
    bool bRoot;

    int iLevel;
    string title;
    string active_path; // the path that is last clicked on/currently selected
    // rootText: text in the root node
    // This is a nasty hack to fix the fact that we can't text out of the rootText
    string rootText;

    Gtk.CellRendererText cell;

    Gtk.TreeIter tree_child_iter;
    Gtk.TreeIter tree_curr_iter;
    Gtk.TreeIter tree_parent_iter;
    Gtk.TreeIter tree_clicked_iter; // the iter matching the item we are clicked on

    Gtk.TreeIter text_child_iter;
    Gtk.TreeIter text_curr_iter;
    Gtk.TreeIter text_parent_iter;

    public VjotsData(){
        tree_store = new Gtk.TreeStore (2, typeof (string), typeof (int));
        tree_store.append (out tree_curr_iter, null);
        text_store = new Gtk.TreeStore (2, typeof (string), typeof (int));
        text_store.append (out text_curr_iter, null);
        view = new Gtk.TreeView.with_model (tree_store);
	view.set_activate_on_single_click(true);
        cell = new Gtk.CellRendererText ();
        bNewTitle = true;
        bFirst = true;
        bRoot = true;
        iLevel = 0;
        active_path = "0"; // by default start at the top of the tree
	rootText = "";
    }

/*
    public Gtk.TreeStore getTreeStore(){
        return tree_store;
    }
*/

    public void setCurrentPath(string path) {
        active_path = path;
        return;
    }

    public Gtk.TreeView getTreeView(){
        return view;
    }

    // Set the title of the current node
    public void setCurrentTitle(string title){
        tree_store.set (tree_curr_iter, 0, title);
        // tree_store.set (text_curr_iter, 0, title);
    }

    public void setTitle(string fn){
        title=fn;
        view.insert_column_with_attributes (-1, "", cell, "text", 0);
        tree_store.set (tree_curr_iter, 0, fn);
        text_store.set (text_curr_iter, 0, "");
    }

    public string getTitle(){
        return title;
    }

    // Check to see if a tag is valid.
    // If we are NOT a valid tag then we're regular text.
    public bool isValidTag(string str) {
        if (str == "\\NewEntry" || str == "\\NewFolder" || str == "\\EndFolder") return true;
	return false;
    }

    // Entry point into our gjots2 parser
    public bool newLine(string str, string fn){
	// We get the last string as a complete empty string
	// ignore it.
        if ("" == str) return false;
	// If we have a valid tag then we don't have root text
	// thus, we set bNewTitle false b/c we'll never need it.
	// Then we process text normally.
        if ( isValidTag(str) ) {
            stdout.printf (" isValidTag TRUE [%s] \n", str);
            bNewTitle = false;
        }
	else {
            stdout.printf (" isValidTag FALSE [%s] \n", str);
	}

	// bNewTitle is true if we have not reached the end of root text
	// OR if our first text is NOT root (which implies we reached
	// the end of our root text.
	if ( true == bNewTitle ) {
            stdout.printf (" newTitle is TRUE [%s] \n", str);
            NewTitle(str);
            return true;
        }
        else {
            stdout.printf (" bNewTitle FALSE [%s] \n", str);
	}

        if (str.match_string("\\NewEntry", false)) NewEntry();
        else if (str.match_string("\\NewFolder", false)) NewFolder();
        else if (str.match_string("\\EndFolder", false)) EndFolder();
        // If we get here, we're a line of text which needs to go into our text_store
        else return AppendText(str);
        return true;
    } // END newLine()

    public bool AppendText(string str){
        stdout.printf("AppendText [%s] \n", str);
        if (iLevel < 0 ) {
            stdout.printf (" AppendText Warn invalid level [%i] \n", iLevel);
            return false;
        }

        if (! text_store.iter_is_valid(text_curr_iter)) {
            stdout.printf (" AppendText() invalid text_curr_iter [%i] \n", (int)text_store.iter_is_valid(text_curr_iter));
            return false;
        }

        string curr="";

	text_store.get (text_curr_iter, 0, &curr, -1);

        if (! text_store.iter_is_valid(text_curr_iter)) {
            stdout.printf (" AppendText() invalid text_curr_iter [%i] \n", (int)text_store.iter_is_valid(text_curr_iter));
            return false;
        }
        // We only need a new line if we currently have text in there
        // otherwise, we want to start at the top
        if ("" != curr) {
            stdout.printf (" AppendText() curr is empty [%s] \n ", curr);
	    curr = curr + "\n"  + str;
        }
        else {
            curr = str;
        }

        stdout.printf (" SUCCESS AppendText() text_store_iter path [%s] string [%s] \n ", text_store.get_string_from_iter(text_curr_iter), str);
	text_store.set (text_curr_iter, 0, curr);
        return true;
    } // END AppendText()

    public void SetTreeStoreText(string str){
        // In gjots2 format, we treat the root text path differently
        // in that we sync the top line of the text with the name of the node.
        if ("0" == active_path) {
            return;
        }
        Gtk.TreePath path = new Gtk.TreePath.from_string(active_path);
        tree_store.get_iter(out tree_clicked_iter, path);

        if (! tree_store.iter_is_valid(tree_clicked_iter)) {
            stdout.printf (" SetTreeStoreText() invalid tree_clicked_iter [%i] \n", (int)text_store.iter_is_valid(tree_clicked_iter));
            return;
        }

        // FRED TODO FIXME THIS FAILS
        // FIGURE OUT WHERE THE PROPER PLACE TO WRITE TO IS AND FIX THIS...

        tree_store.set (tree_clicked_iter, 0, str);
    } // END SetTreeStoreText()

    bool NewTitle(string str){
        stdout.printf (" NewTitle() Level [%i] [%s] \n", iLevel, str);
        tree_store.set (tree_curr_iter, 0, str);
	text_store.set (text_curr_iter, 0, str);
	setRootText(str);
	return false;
    } // END NewTitle()

    void EndFolder(){
        stdout.printf (" NewFolder() Level [%i] \n", iLevel);

        if ((iLevel -1 ) < 0) {
            stdout.printf (" EndFolder() Skipping sprurios EndFolder at Level [%i] \n", iLevel);
            return;
        }
        tree_store.iter_parent(out tree_curr_iter, tree_curr_iter);
        tree_store.iter_parent(out tree_parent_iter, tree_parent_iter);

        text_store.iter_parent(out text_curr_iter, text_curr_iter);
        text_store.iter_parent(out text_parent_iter, text_parent_iter);

	iLevel--;
    } // END EndFolder()

    // Called when we're first initing
    public void firstFolder(){
	tree_store.append (out tree_child_iter, tree_curr_iter);
	text_store.append (out text_child_iter, text_curr_iter);

        stdout.printf ("  NewFolder() Level [%i] Tree Curr Depth: [%i] \n", iLevel, tree_store.iter_depth(tree_curr_iter));
        stdout.printf ("  NewFolder() Level [%i] Text Curr Depth: [%i] \n", iLevel, text_store.iter_depth(text_curr_iter));

	tree_parent_iter=tree_curr_iter;
	text_parent_iter=text_curr_iter;

        tree_curr_iter=tree_child_iter;
        text_curr_iter=text_child_iter;

	iLevel++;
        bFirst=true;
    } // END firstFolder()

    public void NewFolder(){
        stdout.printf ("  NewFolder() Level [%i] Tree Curr Depth: [%i] \n", iLevel, tree_store.iter_depth(tree_curr_iter));
	tree_store.append (out tree_child_iter, tree_curr_iter);
	text_store.append (out text_child_iter, text_curr_iter);

	tree_parent_iter=tree_curr_iter;
	text_parent_iter=text_curr_iter;

        tree_curr_iter=tree_child_iter;
        text_curr_iter=text_child_iter;

        stdout.printf ("  NewFolder() Level [%i] Tree Curr Depth: [%i] \n", iLevel, tree_store.iter_depth(tree_curr_iter));
        stdout.printf ("  NewFolder() Level [%i] Text Curr Depth: [%i] \n", iLevel, text_store.iter_depth(text_curr_iter));

	iLevel++;
        bFirst=true;
    } // END NewFolder()

    public void NewEntry(){
        stdout.printf (" newEntry() BEGIN \n");
        if (! tree_store.iter_is_valid(tree_curr_iter)) {
            stdout.printf (" NewEntry() invalid tree_curr_iter [%i] \n", (int)tree_store.iter_is_valid(tree_curr_iter));
            return;
        }

        if (! text_store.iter_is_valid(text_curr_iter)) {
            stdout.printf (" NewEntry() invalid text_curr_iter [%i] \n", (int)tree_store.iter_is_valid(text_curr_iter));
            return;
        }

        // STUB TODO FIXME
        // Need to do the "right thing" when we're at the root node
        // That is to make a new folder here
        // Until we sort it out, just print out where we are

        if (bRoot)  {
            stdout.printf (" newEntry() root node \n");
            bRoot = false;
            bNewTitle = false;
            NewFolder();
        }

        if (bFirst) bFirst=false;
        else if (0 == iLevel) {
            tree_store.append (out tree_curr_iter, null );
            text_store.append (out text_curr_iter, null );
            tree_parent_iter=tree_curr_iter;
            text_parent_iter=text_curr_iter;
        }
        else {
            tree_store.append (out tree_curr_iter, tree_parent_iter);
            text_store.append (out text_curr_iter, text_parent_iter);
        }

        // bNewTitle = true;
    } // END NewEntry()

    public void setText(Gtk.TreePath path, string str){
        // Gtk.TreeIter tmp_iter;
        // if  ( ! text_store.get_iter_from_string (out tree_clicked_iter, path.to_string()))  {
        if  ( ! text_store.get_iter_from_string (out tree_clicked_iter, active_path))  {
            // TODO FIND OUT WHY THIS IS ACTUALLY WRONG
            stdout.printf (" setText UK error \n");
            return;
        }
        stdout.printf (" setText() [%s] \n", str);
        text_store.set (tree_clicked_iter, 0, str);
    } // END setText()

    public string getText(Gtk.TreePath path){
        Gtk.TreeIter tmp_iter;
        string str="";
        if  ( ! text_store.get_iter_from_string (out tmp_iter, path.to_string())) return str;
	text_store.get (tmp_iter, 0, &str, -1);
	return str;
    } // END getText()

    public bool setRootText(string str){
        Gtk.TreeIter tmp_iter;
        if  ( ! text_store.get_iter_from_string (out tmp_iter, "0")) return false;
	text_store.set (tmp_iter, 0, &str, -1);
	rootText = str;
	return true;
    }

    public string getRootText(){
	return rootText;
	/*
        Gtk.TreeIter tmp_iter;
        string str = "";
        if  ( ! text_store.get_iter_from_string (out tmp_iter, "0")) return str;
	text_store.get (tmp_iter, 0, &str, -1);
	return str;
	*/
    }

    string get_jots_text_recursive(Gtk.TreeIter in_iter){
        string result="";
        Gtk.TreeIter iter = in_iter;

        for (bool next=true; next; next = tree_store.iter_next (ref iter)) {
            string text_entry; // text entry associated with a particular item
	    string str=tree_store.get_string_from_iter(iter);
            Gtk.TreeIter l_iter;
	    text_store.get_iter_from_string(out l_iter, str);
	    text_store.get (l_iter, 0, out text_entry);

            // Skip over the first node b/c that's the
            // filename node and saving it will break our format
            if ("0" == tree_store.get_string_from_iter(iter) ) {
                // result = result + text_entry;
                stdout.printf (" get_jots_text_recursive() ROOT text [%s] \n", text_entry);
                stdout.printf (" get_jots_text_recursive() result text [%s] \n", result);
            }
            else {
                result = result+"\\NewEntry\n" + text_entry + "\n";
            }

            stdout.printf (" path [%s] \n", tree_store.get_string_from_iter(iter));

            if (tree_store.iter_has_child(in_iter)){
                Gtk.TreeIter child;
                if (! tree_store.iter_children(out child, iter) ) continue;

                // Skip over the first node b/c that's the
                // filename node and saving it will break our format
                if ("0" == tree_store.get_string_from_iter(iter) ) {
                    result = result + get_jots_text_recursive(child);
                }
                else {
                    result = result + "\\NewFolder\n" + get_jots_text_recursive(child) + "\\EndFolder\n";
                }
            } // END if ()
        } // END for()

        return result;
    } // END get_jots_text_recursive()

    /* get_jots_text() Get complete text of document in gjots2 format */
    public string get_jots_doc(){
        Gtk.TreeIter iter;
        tree_store.get_iter_first(out iter);
	string text = rootText;
        text = text + "\n" + get_jots_text_recursive(iter);
        stdout.printf (" get_jots_doc() final text [%s] \n", text);
	return text;
    } // END save()

// BEGIN NODE MANIPULATION FUNCTIONS
// These functions are responsible for creating new nodes, etc
    public void createNode( Gtk.TreePath path ){
        // Gtk.TreeViewColumn column = new Gtk.TreeViewColumn();
        Gtk.TreeIter tree_iter, ntree_iter, text_iter, ntext_iter;
        tree_store.get_iter (out tree_iter, path);
        tree_store.append(out ntree_iter, tree_iter); // create tree node

        text_store.get_iter (out text_iter, path);
        text_store.append(out ntext_iter, text_iter); // create text node
        text_store.set (ntext_iter, 0, "New...>>>");
        tree_store.set (ntree_iter, 0, "New...>>>");
    } // END createNode()
// END NODE MANIPULATION FUNCTIONS

} // END VJotsData()
