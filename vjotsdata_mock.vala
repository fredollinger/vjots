class VjotsDataMock : Object {
    public Gtk.TreeStore tree_store; // stores categories in left pane
    Gtk.TreeStore text_store; // stores actual text in right pane
    public Gtk.TreeView view;

    static bool bFirstRun;
    bool bNewTitle;
    bool bFirst;
    int iLevel;
    string title;

    Gtk.CellRendererText cell;

    Gtk.TreeIter tree_child_iter;
    Gtk.TreeIter tree_curr_iter;
    Gtk.TreeIter tree_parent_iter;

    Gtk.TreeIter text_child_iter;
    Gtk.TreeIter text_curr_iter;
    Gtk.TreeIter text_parent_iter;


    public VjotsDataMock(){
        bFirstRun = false;
        tree_store = new Gtk.TreeStore (2, typeof (string), typeof (int));
        tree_store.append (out tree_curr_iter, null);
        text_store = new Gtk.TreeStore (2, typeof (string), typeof (int));
        text_store.append (out text_curr_iter, null);
        view = new Gtk.TreeView.with_model (tree_store);
	view.set_activate_on_single_click(true);
        cell = new Gtk.CellRendererText ();
        bNewTitle = false;
        bFirst = true;
        iLevel=0;
    }

/*
    public Gtk.TreeStore getTreeStore(){
        return tree_store;
    }
*/

    public Gtk.TreeView getTreeView(){
        return view;
    }

    public void setTitle(string fn){
        view.insert_column_with_attributes (-1, fn, cell, "text", 0);
        title=fn;
    }

    public string getTitle(){
        return title;
    }

    public void newLine(string str, string fn){

        stdout.printf (" newLine() first run [%i] \n", (int)bFirstRun);
        if (!bFirstRun)  {
            AppendText(str);
            return;
        }
        stdout.printf (" newLine() string [%s] \n", str);

	bFirstRun = true;

        if ( 0 == str.length ) AppendText(str);
        else if (str.match_string("\\NewEntry", false)) NewEntry();
        else if (str.match_string("\\NewFolder", false)) NewFolder();
        else if (str.match_string("\\EndFolder", false)) EndFolder();
        else if ( bNewTitle ) NewTitle(str);
        // If we get here, we're a line of text which needs to go into our text_store
        else AppendText(str);
    } // END newLine()

    void AppendText(string str){
        stdout.printf (" AppendText() string [%s] \n", str);
        string curr="";
	text_store.get (text_curr_iter, 0, &curr, -1);
	curr = curr + "\n"  + str;
	text_store.set (text_curr_iter, 0, curr);
    } // END AppendText()

    void NewTitle(string str){
        tree_store.set (tree_curr_iter, 0, str);
	text_store.set (text_curr_iter, 0, str);
        bNewTitle=false;
    } // END NewTitle()

    void EndFolder(){
        stdout.printf (" endFolder() level [%i] \n", iLevel);
        tree_store.iter_parent(out tree_curr_iter, tree_curr_iter);
        tree_store.iter_parent(out tree_parent_iter, tree_parent_iter);

        text_store.iter_parent(out text_curr_iter, text_curr_iter);
        text_store.iter_parent(out text_parent_iter, text_parent_iter);

	iLevel--;
    } // END EndFolder()

    void NewFolder(){
	tree_store.append (out tree_child_iter, tree_curr_iter);
	text_store.append (out text_child_iter, text_curr_iter);

	tree_parent_iter=tree_curr_iter;
	text_parent_iter=text_curr_iter;

        tree_curr_iter=tree_child_iter;
        text_curr_iter=text_child_iter;

	iLevel++;
        bFirst=true;
    } // END NewFolder()

    void NewEntry(){
        if (bFirst) bFirst=false;
        else if (0 == iLevel) {
            tree_store.append (out tree_curr_iter, null );
            text_store.append (out text_curr_iter, null );
            tree_parent_iter=tree_curr_iter;
            text_parent_iter=text_curr_iter;
        }
        else {
            tree_store.append (out tree_curr_iter, tree_parent_iter);
            text_store.append (out text_curr_iter, text_parent_iter);
        }

        bNewTitle = true;
    } // END NewEntry()

    public void setText(Gtk.TreePath path, string str){
        Gtk.TreeIter tmp_iter;
        if  ( ! text_store.get_iter_from_string (out tmp_iter, path.to_string())) return;
        text_store.set (tmp_iter, 0, str);
    } // END setText()

    public string getText(Gtk.TreePath path, Gtk.TreeViewColumn column){
        Gtk.TreeIter tmp_iter;
        string str="";
        if  ( ! text_store.get_iter_from_string (out tmp_iter, path.to_string())) return str;
	text_store.get (tmp_iter, 0, &str, -1);
	return str;
    } // END getText()

    string get_jots_text_recursive(Gtk.TreeIter in_iter){
        string result="";
        Gtk.TreeIter iter = in_iter;

        for (bool next=true; next; next = tree_store.iter_next (ref iter)) {
            string text_entry; // text entry associated with a particular item
	    string str=tree_store.get_string_from_iter(iter);
            Gtk.TreeIter l_iter;
	    text_store.get_iter_from_string(out l_iter, str);
	    text_store.get (l_iter, 0, out text_entry);
            result=result+"\\NewEntry\n" + text_entry + "\n";

            if (tree_store.iter_has_child(in_iter)){
                Gtk.TreeIter child;
                if (! tree_store.iter_children(out child, iter) ) continue;
                result = result + "\\NewFolder\n" + get_jots_text_recursive(child) + "\\EndFolder\n";
            } // END if ()
        } // END for()

        return result;
    } // END get_jots_text_recursive()

    /* get_jots_text() Get complete text of document in gjots2 format */
    public string get_jots_doc(){
        Gtk.TreeIter iter;
        tree_store.get_iter_first(out iter);
        string text = get_jots_text_recursive(iter);
	return text;
    } // END save()

// BEGIN NODE MANIPULATION FUNCTIONS
// These functions are responsible for creating new nodes, etc
    public void createNode( Gtk.TreePath path ){
        // Gtk.TreeViewColumn column = new Gtk.TreeViewColumn();
        Gtk.TreeIter tree_iter, ntree_iter, text_iter, ntext_iter;
        tree_store.get_iter (out tree_iter, path);
        tree_store.append(out ntree_iter, tree_iter); // create tree node

        text_store.get_iter (out text_iter, path);
        text_store.append(out ntext_iter, text_iter); // create text node
        text_store.set (ntext_iter, 0, "New...>>>");
        tree_store.set (ntree_iter, 0, "New...>>>");
    } // END createNode()
// END NODE MANIPULATION FUNCTIONS

} // END VJotsData()
