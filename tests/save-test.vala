class SaveTest : GLib.Object {

    // Test setting and getting root text from object VjotsData NOT
    public static void testGetRootTextNot(string origText) {
        VjotsData data_store = new VjotsData();

        data_store.setRootText(origText);
        string rootText = data_store.getRootText();
	string junkText = "this text is trash and won't match";

        stdout.printf ("SaveTest.loadFile() root text: [%s] [%s] \n", rootText, junkText);

        assert_false(junkText == rootText);

        return;
    } // END testGetRootText()

    // Test setting and getting root text from object VjotsData
    public static void testGetRootText(string origText) {
        VjotsData data_store = new VjotsData();

        data_store.setRootText(origText);
        string rootText = data_store.getRootText();

        stdout.printf ("SaveTest.loadFile() root text: [%s] [%s] \n", rootText, origText);

        assert_true(origText == rootText);

        return;
    } // END testGetRootText()

    public static void testIsValidTagNot(string str) {
        VjotsData vjd = new VjotsData();
	bool res = vjd.isValidTag(str);
        // stdout.printf ("isValidTag: [%s] [%i] \n", str, (int)res);
        assert_true(false == res);
    }

    public static void testIsValidTag(string str) {
        VjotsData vjd = new VjotsData();
	bool res = vjd.isValidTag(str);
        stdout.printf ("isValidTag: [%s] [%i] \n", str, (int)res);
        // assert_true(true == res);
    }

    public static void compareFile(string fn) {
        VjotsData data_store = new VjotsData();
        VjotsIO io = new VjotsIO();
        string origText = io.readVjotsFile(fn);

        string[] lines = origText.split ("\n");

	foreach (unowned string str in lines) {
            stdout.printf ("text: [%s] \n", str);
            data_store.newLine(str, fn);
        } // END foreach()

        string copyText = data_store.get_jots_doc();

        io.saveVjotsFile(fn + ".vjots2", copyText);

        stdout.printf ("SaveTest.compareFile() orig text: [%s] copyText [%s] \n", origText, copyText);
        assert_true(origText == copyText);

        return;
    } // END compareFile()

/*
    public static int main(string[] args) {

        Gtk.init (ref args);
        Test.init(ref args);

        Test.add_func ("/vjotsdata/testGetRootText", () => {
            testGetRootText("orig test");
	});

        Test.add_func ("/parse/isValidTag", () => {
            testIsValidTagNot("test1.gjots2");
            testIsValidTag("\\NewFolder");
            testIsValidTag("\\EndFolder");
            testIsValidTag("\\NewEntry");
	});

        Test.add_func ("/load/test1.gjots", () => {
            compareFile("test1.gjots2");
	});

        Test.run();
        return 0;

    }
*/

}
