class Application : Gtk.Window {

        #if DEBUG
            VjotsDataMock data_store;
	#else
            VjotsData data_store;
	#endif

        Gtk.TextView text_view;
        Gtk.TextBuffer text_buffer;
        Gtk.TreePath curr_path;

        Gtk.Dialog popup_dialog;

        // what we save the file as
        string saveTitle;

	public Application () {
		// Sets the title of the Window:
		this.title = "My Gtk.Window";
                this.saveTitle = "";

		// Center window at startup:
		this.window_position = Gtk.WindowPosition.CENTER;

		// Sets the default size of a window:
		this.set_default_size (350, 500);

		// Whether the titlebar should be hidden during maximization.
		this.hide_titlebar_when_maximized = false;

                text_buffer = text_view.get_buffer();

		text_buffer.changed.connect (() => {
		    stdout.printf ("Text Buffer Has Changed \n");
                });

		// Method called on pressing [X]
		this.destroy.connect (() => {
		    stdout.printf ("Bye!\n");

		    // Terminate the mainloop: (main returns 0)
		    Gtk.main_quit ();
		});

                Gtk.Paned vertPane = new Gtk.Paned(Gtk.Orientation.VERTICAL);
		this.add ( vertPane );

	        // BEGIN VERTICAL TOOLBAR
		Gtk.Toolbar vertBar = new Gtk.Toolbar ();
		vertPane.add (vertBar);

		// BEGIN SAVEBUTTON
		Gtk.Image img = new Gtk.Image.from_icon_name ("document-save", Gtk.IconSize.SMALL_TOOLBAR);
		Gtk.ToolButton saveButton = new Gtk.ToolButton (img, null);
		saveButton.clicked.connect (() => {
                    save_file();
		}); // END saveButton.clicked.connect()
		vertBar.add (saveButton);
		// END SAVEBUTTON

		// BEGIN OPENBUTTON
		img = new Gtk.Image.from_icon_name ("open-save", Gtk.IconSize.SMALL_TOOLBAR);
		Gtk.ToolButton openButton = new Gtk.ToolButton (img, null);
		vertBar.add (openButton);
		// END OPENBUTTON
	        // END VERTICAL TOOLBAR

		#if DEBUG
                    data_store = new VjotsDataMock();
		#else
                    data_store = new VjotsData();
		#endif

		curr_path = new Gtk.TreePath.from_indices(0);

                Gtk.Paned horiPane = new Gtk.Paned(Gtk.Orientation.HORIZONTAL);

                // FIXME: THIS IS A VERTICAL BAR. SIGH.
	        // BEGIN HORIZONTAL TOOLBAR
		Gtk.Toolbar horiBar = new Gtk.Toolbar ();
                horiBar.orientation = Gtk.Orientation.VERTICAL;
		horiPane.add (horiBar);


		// string path = "/reyv/resources/battery-icon.jpg";
		// Gtk.Image image = new Gtk.Image.from_resource(path);


                // TODO LOAD IMAGES FROM RESOURCE
                // CURRENTLY BROKEN

                // string newChildPath = "/vjots/resources/battery-icon.jpg";
		string newChildPath = "/reyv/resources/gjots2-new-child.png";
                // Gdk.Pixbuf pixbuf = new Gdk.Pixbuf.from_file(newChildPath);

                // resources
		//string newChildPath = "/gjots2/resources/gjots2.png";
		img = new Gtk.Image.from_resource(newChildPath);
		// img = new Gtk.Image.from_icon_name ("go-top", Gtk.IconSize.SMALL_TOOLBAR);
		Gtk.ToolButton newChildButton = new Gtk.ToolButton (img, null);
		newChildButton.clicked.connect (() => {
                    data_store.createNode(curr_path);
                }); // END newChildButton.clicked.connect()
		horiBar.add (newChildButton);


		img = new Gtk.Image.from_icon_name ("go-next", Gtk.IconSize.SMALL_TOOLBAR);
		Gtk.ToolButton inOneLevelButton = new Gtk.ToolButton (img, null);
		// newChildButton.clicked.connect (() => {
                //    data_store.createNode(curr_path);
                // }); // END newChildButton.clicked.connect()
		horiBar.add (inOneLevelButton);

	        // END HORIZONTAL TOOLBAR

                Gtk.Paned textTreePane = new Gtk.Paned(Gtk.Orientation.HORIZONTAL);

                Gtk.ScrolledWindow treeWindow = new Gtk.ScrolledWindow( null, null);
		treeWindow.add (data_store.getTreeView());
                Gtk.ScrolledWindow textWindow = new Gtk.ScrolledWindow( null, null);

		data_store.getTreeView().row_activated.connect(row_activated_callback);

                text_view = new Gtk.TextView ();
                text_buffer = text_view.get_buffer();

		text_buffer.changed.connect(text_changed_callback);

		text_view.set_wrap_mode (Gtk.WrapMode.WORD);
		textWindow.add (text_view);

		textTreePane.set_position(200);
		textTreePane.add(treeWindow);
		textTreePane.add(textWindow);

		horiPane.add(textTreePane);
		vertPane.add ( horiPane );
		horiPane.set_position (50);

	} // END Application ()

        void save_file() {
            // Save the text box first into the data_store for later.
            // This is in case they edit the text, but don't move the focused item.
            // If we don't update the text in this case, we won't save the latest text in the text view.
            data_store.setText(curr_path, text_view.buffer.text);

            string text = data_store.get_jots_doc();

            try{
                FileUtils.set_contents (data_store.getTitle(), text);
     }
            catch (GLib.FileError e) {
                stderr.printf ("%s\n", e.message);
                return;
            }
     finally{}

            popup_dialog = new Gtk.Dialog.with_buttons ("File saved successfully", this,
                                                  Gtk.DialogFlags.MODAL,
                                                  Gtk.Stock.OK,
                                                  Gtk.ResponseType.OK, null);

            var label = new Gtk.Label ("File saved successfully");
            popup_dialog.get_content_area().add(label);

            popup_dialog.response.connect (destroy_popup_dialog);

            popup_dialog.show_all ();
        }

        void destroy_popup_dialog() {
            popup_dialog.destroy();
        }

        void text_changed_callback (){
            string[] strs = text_view.buffer.text.split ("\n");
            // stdout.printf ("text_changed_callback() %s \n", strs[0]);
            data_store.SetTreeStoreText(strs[0]);
        }

        /* When a user clicks on a topic show the text */
        void row_activated_callback (Gtk.TreePath path, Gtk.TreeViewColumn column){
             // save the text box first into the data_store for later
             data_store.setText(curr_path, text_view.buffer.text);

             // save the current path for next time
             curr_path=path;
             data_store.setCurrentPath(path.to_string());
             stdout.printf ("current path: %s \n", path.to_string());


             // Load the old text in the currently highlighted item into the text box
 	    text_view.buffer.text = data_store.getText(path);
        } // END row_activated_callback()

        public void blankNote() {
            data_store.setTitle("gjotsfile");
            // data_store.newLine("This text is in the title", "test2.gjots2");
            //data_store.newLine("\\NewEntry", "test2.gjots2");
            //data_store.newLine("0:0", "test2.gjots2");
            data_store.AppendText("This text is in the title.");
            // data_store.NewEntry();
            // data_store.setCurrentTitle("Title 1");
            this.saveTitle = "";
        }

    public static int main (string[] args) {
        Gtk.init (ref args);

        Application app = new Application ();
        app.show_all ();
        app.blankNote();

        Gtk.main ();
        return 0;
    } // END main()
} // END class Gtk.Window
