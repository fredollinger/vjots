/*
class Parser : Gtk.Window {

    VjotsData data_store;

    public Parser () {
        VjotsData data_store = new VjotsData();
	this.add(data_store.getTreeView());
    }
}
*/

// Code to test the actual parser from 1st principles
public static int main(string[] args) {
    Gtk.init (ref args);

    VjotsData data_store = new VjotsData();
    data_store.newLine("This text is in the title", "test2.gjots2");
    data_store.newLine("\\NewEntry", "test2.gjots2");
    data_store.newLine("0:0", "test2.gjots2");

    // Parser app = new Parser();
    // app.show_all ();
    // Gtk.main ();
    return 0;
}
