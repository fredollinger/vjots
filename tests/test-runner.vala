public static int main(string[] args) {
    Gtk.init (ref args);
    Test.init(ref args);

    Test.add_func ("/parse/isValidTag", () => {
            SaveTest st = new SaveTest();
            st.testIsValidTagNot("test1.gjots2");
            st.testIsValidTag("\\NewFolder");
            st.testIsValidTag("\\EndFolder");
            st.testIsValidTag("\\NewEntry");
    });

    Test.add_func ("/load/test1.gjots", () => {
        SaveTest st = new SaveTest();
        st.compareFile("test1.gjots2");
    });

    Test.add_func ("/load/test2.gjots", () => {
        SaveTest st = new SaveTest();
        st.compareFile("test2.gjots2");
    });

    Test.run();
    return 0;
}
